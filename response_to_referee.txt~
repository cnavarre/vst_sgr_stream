1. A suggested further bit of analysis involves the possibility of 
using the Blue Horizontal Branch (BHB) to Blue Straggler (BS) star 
ratio to try and determine if all of these five stream pieces all 
originate with the Sagittarius dwarf.  Sagittarius has a 
distinctive BHB/BS ratio in other locations along the stream, and 
it would be useful to measure the ratio in the data presented here.  
Do the authors have enough signal-to-noise and depth to 
compare BHB/BS numbers for both the bright and faint stream pieces 
to say, for instance, if the BHB/BS ratio matches that seen elsewhere 
along the Sag. dwarf tidal tails and perhaps also to compare with 
the ratio seen along the Cetus Polar stream?  If this can be done 
using the VISTA Atlas ugr photometry presented in section 3.3 and 
Figure 8, this may help determine, for instance if the stream 2 in 
the faint branch of the stream is associated with Sagittarius.
             
        We agree with the referee that this is a great idea in
        principle! However, there is one stumbling block that would
        require substantially more in-depth analysis, which we would
        prefer to carry out elsewhere. The problem is that the VST
        ATLAS g, r and especially u bands do not match their
        counterparts in SDSS. For the u-band photometry, the situation
        is exacerbated by the fact that the APASS does not provide the
        u band data and therefore the u-band calibration is not as
        secure as for the other bands. Even though the differences
        between the VST and SDSS filters are small, they are ample
        enough to influence the distribution of BHB and BS stars in
        u-g, g-r space and change the exact position of the boundary
        between the two classes. What has become clear from our
        preliminary study, is that because the decision boundaries do
        not match exactly, the measured ratio BHB/BS in the part of
        the sky covered by the VST ATLAS does not have to match that
        in the SDSS part of the sky even if the stellar populations
        are identical. We therefore leave the careful examination of
        the behavior of the BHB/BS populations in the VST ATLAS bands
        to a future publication. Importantly, the BHB analysis
        presented here is not the focus of the paper, it is simply an
        additional check of the line-of-sight distribution of the
        debris - a check that can be done without a perfect match of
        the BHB/BS boundaries in the VST and SDSS surveys. With
        regards to the confusion with the Cetus stream, we have tried
        to clarify in the paper that the structures we have uncovered
        can not be linked to the Cetus stream as it clearly follows a
        very different track in 3D. We have now added lines showing
        distance gradients for the Sgr and the Cetus streams to Figure
        8, aiming to emphasize this distinction.

Some minor points:

2. Can the authors specify a significance of the various bit of the 5
streams.  For instance, looking at Figure 4, I would not think that
SGR 1b in the rightmost panel is significant. As Poisson statistics on
top of a model subtracted background suggests sqrt(N) = sqrt(40) = 6
counts or perhaps a bump of 1-sigma significance.  I understand that a
possible detection in BHBs increases the overall significance of this,
but still it's rather weak.  Can the authors give a quantitative
significance to each of the 5 possible streamlets?

        This an important point that we did not address in the first
        version of the draft. Certainly, the SGB 1b is the less
        significant of all the detections but, as the referee
        mentioned, the detection using BHBs gives more reliability to
        this peak.  We have now added the significance, in sigmas, for
        the SGB and BHB detections independently and commented about
        the low significance of the detections in the faint stream
        but, at the same time, the fact that we also recovered the
        peaks at the right distance using BHBs.

        The two peaks found in the bright Sgr arm are both over 5 sigma 
        signifncant, both in BHB and SGB. These two sub-structures which 
        compose the "fork" are highly significant.

        For the faint arm, the significances are lower. Nonetheless, 
        using BHBs, the significance of the peaks 1a and 1b considered 
        as only one peak, is greater than 10sigma, while the secondary 
        peak has 4sigma of significance. As in the previous case, it seems 
        that there is a "fork" in the stream near the Sgr dwarf core.

3. page 2: Erkal et al (2016b) is referenced.  Is this the same as
Erkal (2016)? or is this a different (in preparation?) paper?  Please
clarify.

        Thank you. The two papers are indeed different ones, and Erkal
        et al. (2016a) was not included in the bibliography. In this
        new version, each paper is cited properly.

4. Figures 5,6 and 7 re-normalize the count density based on the total
        in each Figure.  Somewhere the total number of stars 
        between 5, 6 and 7 should be given, or alternatively, these 
        three figures should be put on the same overall scale. 
        The relative counts can be estimated from Figure 4 somewhat, 
        but it should be made more explicit in perhaps the captions 
        to Figure 5, 6 and 7.  There are many many more stars in Figure
        5 than 6, and that should be made explicit.

        Along these same lines, on page 6, section 3.1,
        coefficients A, B and C are fit on a Lambda-by-Lambda basis to
        each cell.  Can the authors describe how smoothly or roughly
        these coefficients varied?  This will give some indication if
        there is a danger of overfitting the background or not. What
        range did A,B and C take on?

                As you said, Figure 6 contains much less stars than Figure 5
                and that was not completely evident given the previous 
                color-scale of the figures. To put them in the same scale, 
                we divided each bin by the binsize (which is larger in Figure 6 
                than in fig. 5). Figures 5,6 and 7 (as well as the text) 
                were changed accordingly.


        **PENDING**


5. In Figure 8 (upper right panel), the dotted line from Cetus stream
to the grey-scale area is not clear where it is pointing.  That should
be redone or more exactly say in the text where the Cetus stream is in
(Lambda,m-M) space.
        
        We have added two lines to Figure 8 parallel to the Sgr
        trailing debris and the Cetus stream. The caption and the text
        were updated accordingly.

        These lines also emphasizes the fact that while the streams
        cross briefly at Lambda=-100, they follow very different
        tracks in 3D. It is now obvious from the Figure that there is
        very little overlap between the two structures at most
        locations.

6. In tables one and two, sigma(m-M) quantities are given at every
Lambda.  Can these sigmas be interpreted as intrinsic dispersions in
the distribution of star distances or are they strictly measurement
errors?  I.E. can the authors make a case that if the dispersion is
higher, then there's more of a chance that the stars are spread out
along the line of sight?  If so, which streamlets have the most
dispersion?  The authors do discuss this in the text, but perhaps it
could be emphasized a bit more.

        The sigma(m-M) quantities listed in tables 1 and 2 are
        measurement errors for the center of the (m-M) distribution
        and do not represent the dispersion of the peaks. The
        dispersion of star distances could be represented by the
        standard deviation of the Gaussians fitted to the
        distributions, which are of the order of 0.2 mag for the faint
        stream and 0.3 mag for the peaks found in the bright arm.

        We emphasize in the text that the listed errors are in the
        distance and do not account for the dispersion along the line
        of sight.

        **"The authors do discuss this in the text" where?


7. Figure 10. caption, perhaps insert "Left:"  at the beginning of the
        caption to distinguish from the "Right:" marking later.

        This has been corrected.

8. Figure 11. can the authors estimate the Z_SGR distance of the left
and right panel stars, or give a range.  I understand that the left
panel has Z_SGR < 0 (B<0?), but can the authors put a kpc range on
these, say -3 kpc < Z_SGR < 0 and similarly for the right (1 < Z_SGR <
5 kpc) or something like that. Could also be made more explicit in the
text how the positive and negative B values (in degrees) correspond to
Z_SGR distances (in kpc).

        We realized that the third coordinate Z_SGR is not explicitly
        included in the analysis of Figure 11, as the streams are
        confined to a region above/below the Sgr's plane. In this new
        version, the kpc range above/below the Sgr plane is now
        included in the text.

	We thank the referee for suggesting this. We realized that the third 
        coordinate Z_SGR is not explicitly included in the analysis of Figure 11.
        In this new version, the kpc range above/below the Sgr plane is now 
        included in the text, in the last paragraph of section 4.2. 


9. Figure 12 caption.  Last sentence: Both profiles show significant
peak around B= 0..  That should be clarified.  Does this refer to only
the middle panel in Figure 12?  Or all three panels or the right
panel?  It doesn't seem to be strongly true in the right or left
panels only the middle.

        We agree with the referee that this sentence was rather
        confusing. We have now expanded and clarified this portion of
        the Figure caption.
